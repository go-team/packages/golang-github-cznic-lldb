module modernc.org/lldb

go 1.16

require (
	modernc.org/fileutil v1.1.2
	modernc.org/internal v1.0.8
	modernc.org/mathutil v1.5.0
	modernc.org/sortutil v1.1.1
	modernc.org/zappy v1.0.9
)
